# Supplementary material for SpiceFP approach: An exploratory penalized regression to identify combined effects of temporal variables - Application to agri-environmental issues

**Authors: Bénedicte Fontez, Patrice Loisel, Thierry Simonneau, Nadine Hilgert**


# File composition

- README.R               : main file. Contains an application of SpiceFP approach.

- SpiceFP materials.Rproj : the project to open with Rstudio

- Data4spiceFP.RData      : data. rdc_if1_cible and tpr_if1_cible contains 2 temporal predictors (irradiance, temperature) obseved from sunrise to 12H a.m. during 7 days, Y_nc contains Ferari index difference during 7 days (24H*7=168H).

- supmaterials.RData      : running the example takes about 2.1 hours with 4 cores. The file supmaterials.RData can be directly loaded and thus you can go directly to the result visualization from line 130.

- R file                  : contains the linbreaks function and small modifications of two functions of the SpiceFP package (mainly a simplification of the saved results files). 

- add-ins                  : contains the datasets (beta coefficients and X matrix) used for the simulation.


**Objective of the use case** : find the irradiance-temperature combinations that influence the variation of the Ferari index in the morning (sunrise to 12H a.m.).

               
## Packages
```{r}
library(tidyverse)
library(hms)
library(SpiceFP)
library(latex2exp)
library(gridExtra)
library(genlasso)
library(doParallel)
library(ggrepel)
library(purrr)
```


## Functions 
```{r}
# Charger des fonctions modifiées par BF
source("R/evalcandidate.R")
source("R/spicefp.R")
source("R/linbreaks.R")
```

## Data
Load data from Data4spiceFP.RData file 

```{r}
load("Data4spiceFP.RData")
```

## Temporal Predictors

- Irradiance
```{r}
m_rdc_if1_cible<-log(as.matrix(Irradiance[,-c(1)]))
c_rdc_if1_cible<-c(m_rdc_if1_cible)
```

- Temperature
```{r}
m_tpr_if1_cible<-as.matrix(Temperature[,-c(1)])
c_tpr_if1_cible<-c(m_tpr_if1_cible)
```

- Response variable (centered)
```{r}
Y<-FerariIndex_Difference$fi_dif-mean(FerariIndex_Difference$fi_dif)
names(Y) <- rownames(FerariIndex_Difference)
```

```{r}
# Plot
# Layout to split the screen
layout(mat = matrix(c(1,2),2,1, byrow=TRUE),  height = c(2,8))
 
# Draw the boxplot and the histogram 
par(mar=c(0, 3.1, 1.1, 2.1))
boxplot(Y , horizontal=TRUE , ylim=c(-0.6,0.4), xaxt="n" , col=rgb(0.8,0.8,0,0.5) , frame=F)
par(mar=c(4, 3.1, 1.1, 2.1))
hist(Y , breaks=6 , col=rgb(0.2,0.8,0.5,0.5) , border=F , main="" , xlab="Centered values of the Ferari Index differences",
ylab="",
xlim=c(-0.6,0.4))
```

## Implementation of the SpiceFP approach for two explanatory temporal variables: one to be partitioned according to a linear scale (temperature) and another to be partitioned according to a linear scale (irradiance)

```{r}
# Parameterization of the candidate matrices
tpr.nclass = 10:30
irdc.nclass = 10:30
p2<-expand.grid(tpr.nclass, irdc.nclass)
parlist.tpr<-split(p2[,1], seq(nrow(p2)))
parlist.irdc<-split(p2[,2], seq(nrow(p2)))
```

**Remark** : The implementation of this example requires approximately 2.168693 hours with 2.30GHz × 4 cores (12th Gen Intel(R) Core(TM) i7-12700H). If you don't want to run it, you can load file supmaterials.RData (load("supmaterials.RData")) and continue the running

The parameterization of this example does not rake wide, just to save execution time. In fact,it is important to rake wide when you don't have expert knowledge. 

```{r}
  # starting time
  start_time_ <- Sys.time()
  
  # Process with SpiceFP
  res_simu <- spicefp2(y=Y,
                       fp1  = m_tpr_if1_cible, 
                       fp2  = m_rdc_if1_cible,
                       fun1 = linbreaks, 
                       fun2 = linbreaks,
                       parlists=list(parlist.tpr,
                                     parlist.irdc),
                       K = 1,
                       penratios = c(1/9, 1/3, 0, 1, 3, 9),
                       nknots = 20,
                       xcentering = FALSE,
                       xscaling = FALSE,
                       ncores = 4,
                       write.external.file = FALSE
                       )
  # Ending time
  duration_ <- Sys.time() - start_time_
```

## Results

```{r}
load("supmaterials.RData")
```
- Extraction of the parameter information

```{r}
# Fine-mesh parameter output from spicefp
coef <- res_simu[["spicefp.coef"]]

# Remove NA columns of log-Irradiance
aelim=NULL
for (j in 1:900){if(sum(is.na(coef[,j])) == 900){aelim=c(aelim,j)}}
coef2 <- coef[,-c(aelim)]

# Creation of a data.frame to plot
beta<-as.vector(coef2)
Var1 <- rep(as.numeric(colnames(coef2)),
            each=nrow(coef2)) #LogI
Var2 <- rep(as.numeric(rownames(coef2)),
            ncol(coef2)) # Temp

beta_plot <- data.frame(beta,Var1,Var2)
```

- Visualization of the coefficients selected

```{r}
p_beta_ <-
  ggplot(beta_plot, 
         aes(x = Var1 , y = Var2, fill=beta)) + 
  geom_raster() +
  labs(x="Irradiance (mmol/m²/s)",
       y="Temperature (°C)",fill = " Coef") + 
  theme(plot.title = element_text(size=20,hjust = 0.5),axis.text.x = element_text(vjust=1, size = 8, hjust = 1),
        plot.subtitle = element_text(size=16,hjust = 0.5))+
  scale_fill_gradient2(low = "blue",high="red",mid="white",na.value="black",limits=c(-0.03,0.011),
                       breaks =seq(-0.03,0.03,0.01)) +
  scale_x_continuous( breaks = seq(min(beta_plot$Var1, na.rm = T)-0.02, 
                                   max(beta_plot$Var1, na.rm = T)+ 0.02,length.out = 10), 
                      label = as.character(round(exp(seq(min(beta_plot$Var1, na.rm = T)-0.02,
                                                         max(beta_plot$Var1, na.rm = T)+0.02,length.out = 10)),0)),
                      limits=range(seq(min(beta_plot$Var1, na.rm = T)-0.02,max(beta_plot$Var1, na.rm = T)+0.02,length.out = 10))) +
  scale_y_continuous(breaks = seq(15,50,5), limits=range(beta_plot$Var2)+c(-0.1,0.1)) + 
  theme(axis.text.x = element_text(size = 10,face="bold", colour = "black"), 
        axis.text.y = element_text(size = 10,face="bold", colour = "black"), 
        axis.title.x =element_text(size = 13,face="bold", colour = "black"), 
        axis.title.y =element_text(size = 13,face="bold", colour = "black"), 
        legend.text =element_text(size = 10,face="bold", colour = "black"), 
        legend.title = element_text(size = 10,face="bold", colour = "black")) + 
  theme(panel.background=element_rect(fill="transparent",colour=NA), 
        panel.grid.major = element_line(colour = "white"))
 
```
**Remark : SpiceFP is programmed to do more iterations on request (option max_iter) using residuals as new explanative variable**

# Quality of fit 
```{r}
Y_hat <- as.vector(res_simu[["Evaluations"]][[1]][["XBeta"]])
```

## Data for ggplot
```{r}
ydata <- data.frame(y_obs=Y, y_hat=Y_hat) 
```

## Visualization
```{r}
p_ydata <- ggplot(data = ydata, aes(y = y_hat , x = y_obs)) + geom_point(size=2) + 
  geom_smooth(method='lm',formula=y~x,size=2,level=0) + geom_abline(mapping=aes(slope=1, intercept=0),
                                                                    colour="red", linetype= 2, linewidth=2) + 
  labs(y =bquote(hat("Y")), x = "Y", 
       subtitle = paste("slope = ",round(lm(ydata$y_hat~ydata$y_obs)$coefficients[2],3)), 
       title="Quality of fit") + theme(plot.title = element_text(size=20,hjust = 0.5),
                                       axis.text.x = element_text(vjust=1,size = 8, hjust = 1),
                                       plot.subtitle = element_text(size=16,hjust = 0.5))+
  theme(axis.text.x = element_text(size = 10,face="bold", colour = "black"), 
        axis.text.y = element_text(size = 10,face="bold", colour = "black"), 
        axis.title.x =element_text(size = 13,face="bold", colour = "black"), 
        axis.title.y =element_text(size = 13,face="bold", colour = "black"), 
        legend.text =element_text(size = 10,face="bold", colour = "black"), 
        legend.title = element_text(size = 10,face="bold", colour = "black") )+
  theme(panel.background=element_rect(fill="white", colour = "white")) + 
  scale_x_continuous(limits=c(-1,1)) + scale_y_continuous(limits=c(-1,1))
p_ydata
```

# Residuals histogram

```{r}
residuals <- Y-Y_hat
```

## Normality of residuals
```{r}
shapiro.test(residuals)
```

## Visualisation
```{r}
p_residuals<-ggplot(data=data.frame(residuals), aes(residuals)) +  geom_histogram(bins =18,color="black", fill="white") +  
  labs(x="Residuals",y="Frequency",title = "Residuals histogram") + 
  theme(plot.title = element_text(size=20,hjust = 0.5),
        axis.text.x = element_text(vjust=1, hjust = 1,size = 10,face="bold", colour = "black"),
        plot.subtitle = element_text(size=16,hjust = 0.5))+
  theme(axis.text.y = element_text(size = 10,face="bold", colour = "black"), 
        axis.title.x =element_text(size = 13,face="bold", colour = "black"), 
        axis.title.y =element_text(size = 13,face="bold", colour = "black"), 
        legend.text =element_text(size = 10,face="bold", colour = "black"), 
        legend.title = element_text(size = 10,face="bold", colour = "black"))+
  theme(panel.background=element_rect(fill="white", colour = "white"))+ 
  scale_x_continuous(limits=c(-1,1)) + 
  scale_y_continuous(limits=c(0,10))
p_residuals
```

# 1$\%$ best models

- Extraction of the coefficients of 1$\%$ best models

```{r}
nb.model <- 21*21*6*20 # n_T * n_log(I) * nb penratio * nknots
One.percent <- trunc(nb.model/100)
c.crit <- coef_spicefp(res_simu, iter_=1,
                       criterion ="AIC_",nmodels=One.percent,
                       ncores = 4,
                       write.external.file =FALSE)
#Be carefull : c.crit is 13 Gigabytes
```

- Computation of the mean of the coefficients
```{r}
# Mean of the fine-mesh coefficients
coef.1P <- c.crit[["coef.list"]][[1]][["Candidate.coef.NA.finemeshed"]]
vectcoef <- as.vector(coef.1P)
effectif <- as.numeric(!is.na(vectcoef))
for (i in 2:One.percent){
vectcoefnew <- as.vector(c.crit[["coef.list"]][[i]][["Candidate.coef.NA.finemeshed"]])
matint <- cbind(vectcoef,vectcoefnew)
vectcoef <- apply(matint,1,sum,na.rm=TRUE)
effectif <- effectif+as.numeric(!is.na(vectcoefnew))
}
rescoef <- vectcoef/effectif
```
